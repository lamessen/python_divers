#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets
import ihm  # import du fichier ihm.py généré par pyuic5
from random import randint



class MyWindow(QtWidgets.QMainWindow):
    
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.ui = ihm.Ui_Lettres()
        self.ui.setupUi(self)
        
        # Appel des méthodes au click sur les boutons
        self.ui.reinit.clicked.connect(self.reinitialisation)
        self.ui.consonne.clicked.connect(self.consonne)
        self.ui.voyelle.clicked.connect(self.voyelle)
        self.compteur = 1
        self.ui.lettre_1.setText('')
        self.ui.lettre_2.setText('')
        self.ui.lettre_3.setText('')
        self.ui.lettre_4.setText('')
        self.ui.lettre_5.setText('')
        self.ui.lettre_6.setText('')
        self.ui.lettre_7.setText('')
        self.ui.lettre_8.setText('')
        self.ui.lettre_9.setText('')
        self.ui.lettre_10.setText('')
        
        #Compteur de lettre
        self.compteur = 1
        

    def reinitialisation(self):
        self.compteur = 1
        self.ui.lettre_1.setText('')
        self.ui.lettre_2.setText('')
        self.ui.lettre_3.setText('')
        self.ui.lettre_4.setText('')
        self.ui.lettre_5.setText('')
        self.ui.lettre_6.setText('')
        self.ui.lettre_7.setText('')
        self.ui.lettre_8.setText('')
        self.ui.lettre_9.setText('')
        self.ui.lettre_10.setText('')

    def consonne(self):
        if self.compteur <= 10:
            consonnes = 'BCCCDDDDFFGHJKLLLLLMMMNNNNNNPPPQRRRRRRRSSSSSSTTTTTTVVWXZ'
            eval("self.ui.lettre_{}.setText('{}')".format(self.compteur,consonnes[randint(0,len(consonnes)-1)]) )
            self.compteur += 1

                
    def voyelle(self):
        if self.compteur <= 10:
            voyelles = 'AAAAAAAEEEEEEEEEEEEIIIIIIIOOOOOUUUUY'
            eval("self.ui.lettre_{}.setText('{}')".format(self.compteur,voyelles[randint(0,len(voyelles)-1)]) )
            self.compteur += 1
        
        
# Lancement de l'IHM si script lancé directement
if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
