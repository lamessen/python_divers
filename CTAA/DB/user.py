#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
gestion de l'utilisateur - Stade de tests

@author: nico
"""

import hashlib
from DBlite import DBlite
from getpass import getpass

class User:
            
    def __init__(self):
        self.db = DBlite()
        self.user =''
        self.password  = ''
        self.identification = False
        
        
    def connect_user(self):
        # Récupération des infos de connexion
        user_info = str(input("Utilisateur :"))
        password_info = getpass("Mot de passe :")
        password_info = hashlib.sha256(password_info.encode()).hexdigest()
        
        #Lecture de la BDD
        try:
            pass_db = self.db.select_data('USER','Password',"Login='{}'".format(user_info))
        except :
            print("Le nom d'utilisateur est incorrect")
        
        # Contrôle du Mdp
        if pass_db == password_info:
            print("Identification confirmée")
            self.user = user_info
            self.password  = password_info
            self.identification = True
        
        
        
        
        

        





