#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classe de connexion SQLite à la Base de données
"""
import sqlite3

class DBlite:
            
    def __init__(self):
        self.adresse = './BDD.db'
        self.conn  = sqlite3.connect(self.adresse)
        self.cursor = self.conn.cursor()      


    def close_db(self):
        self.conn.close()
    
    
    def select_data(self,table,recherche='*',critere=''):
        ''' Lecture de données
        table     : nom de la table
        recherche : valeur à lire sous la forme "champs='Valeur'" ou '*'
        critere   : critère de recherche (forme idem)
        '''
        commande = "SELECT {} FROM {}".format(recherche,table)
        if critere:
            commande = ' '.join([commande,'WHERE ',critere])
            
        try:    
            self.cursor.execute(commande)
        except sqlite3.OperationalError as err:
            print("Erreur :" + str(err))

        # Traitement résultat   
        result = []
        for elem in self.cursor:
            result.append(elem)
        
        for i_elem in range(len(result)):
            if len(result[i_elem]) == 1:
                result[i_elem] = result[i_elem][0]
        if len(result) == 1:
            result = result[0]
        return result
    
  
    def update_data(self,table,modification,critere=''):
        ''' Mise à jour de champs dans la table
        table         : nom de la table
        modification  : valeur à corriger sous la forme "champs='Valeur'"
        critere       : critère des champs à corriger (forme idem)
        '''
        commande = "UPDATE {} SET {}".format(table,modification)
        if critere:
            commande = ' '.join([commande,'WHERE ',critere])
            
        try:    
            self.cursor.execute(commande)
        except sqlite3.OperationalError as err:
            print("Erreur :" + str(err))
        self.conn.commit()
        
    def insert_data(self,table,champs,valeurs):
        ''' Insertion de données dans la table
        table   : nom de la table
        champs  : liste des champs à modifier espacés par des virgules
        valeurs : liste des valeurs correspondants (virgules)
        
        '''
        commande = "INSERT INTO {} ({}) VALUES ({})".format(table,champs,valeurs)
        try:    
            self.cursor.execute(commande)
        except sqlite3.OperationalError as err:
            print("Erreur :" + str(err))
        self.conn.commit()