## Conversion du .ui en .py
pyuic5 -x nom.ui -o nom.py
pyrcc5 source.qrc -o source.py

## Création d'un standalone pour Windows

* Installer pyinstaller
'pip install pyinstaller'

* Création du .exe
'pyinstaller --noconsole --onefile lettres_main.py'